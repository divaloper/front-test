import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatTabsModule,
  MatDialogModule,
} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { LancamentoService } from './services/lancamento.service';
import { AppComponent } from './app.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { FaturasComponent } from './pages/historico-geral/faturas.component';
import { HistoricoPorMesComponent } from './pages/historico-por-mes/historico-por-mes.component';
import { HistoricoPorCategoriaComponent } from './pages/historico-por-categoria/historico-por-categoria.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    FaturasComponent,
    HistoricoPorMesComponent,
    HistoricoPorCategoriaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    MatDialogModule,
  ],
  providers: [LancamentoService],
  bootstrap: [AppComponent],
})
export class AppModule {}
