import { Component, OnInit } from '@angular/core';
import { LancamentoService } from '../../services/lancamento.service';

@Component({
  selector: 'app-faturas',
  templateUrl: './faturas.component.html',
  styleUrls: ['./faturas.component.css'],
})
export class FaturasComponent implements OnInit {
  public lancamentos = new Array<any>();
  public displayedColumns: string[] = ['geral'];
  public dataSource = [];

  constructor(private lancamentoService: LancamentoService) {}

  ngOnInit() {
    this.getAsyncData();
  }

  async getAsyncData() {
    this.lancamentos = await this.lancamentoService.getLancamentos();
    this.dataSource = this.lancamentos;
  }
}
