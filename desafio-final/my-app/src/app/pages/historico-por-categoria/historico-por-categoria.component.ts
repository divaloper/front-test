import { Component, OnInit } from '@angular/core';
import { LancamentoService } from '../../services/lancamento.service';

@Component({
  selector: 'app-historico-por-categoria',
  templateUrl: './historico-por-categoria.component.html',
  styleUrls: ['./historico-por-categoria.component.css'],
})
export class HistoricoPorCategoriaComponent implements OnInit {
  public lancamentos = new Array<any>();
  public categorias = new Array<any>();
  public consolidado = new Array<any>();
  public displayedColumns: string[] = ['categoria', 'valor'];
  public dataSource = [];

  constructor(private lancamentoService: LancamentoService) {}

  ngOnInit() {
    this.getAsyncData();
  }
  async getAsyncData() {
    this.lancamentos = await this.lancamentoService.getLancamentos();
    this.categorias = await this.lancamentoService.getCategorias();

    this.geraConsolidado();
  }

  public geraConsolidado() {
    for (let i = 1; i < this.categorias.length; i++) {
      const filtraLancamentos = (lancamentos) => lancamentos.categoria - 1 === i;
      const lancamentosFiltrados = this.lancamentos.filter(filtraLancamentos);

      var resultado = lancamentosFiltrados.reduce(function (soma, valorFinal) {
        return soma + valorFinal.valor;
      }, 0);
      if (resultado > 0) {
        this.consolidado.push({
          categoria: this.retornaCategoria(i),
          valor: resultado,
        });
      }
    }
    this.dataSource = this.consolidado;
  }
  public retornaCategoria(categoria): string {
    return this.categorias[categoria].nome;
  }
  public retornaMes(mes): string {
    const mesesDoAno = [
      { id: 0, nome: '' },
      { id: 1, nome: 'Janeiro' },
      { id: 2, nome: 'Fevereiro' },
      { id: 3, nome: 'Março' },
      { id: 4, nome: 'Abril' },
      { id: 5, nome: 'Maio' },
      { id: 6, nome: 'Junho' },
      { id: 7, nome: 'Julho' },
      { id: 8, nome: 'Agosto' },
      { id: 9, nome: 'Setembro' },
      { id: 10, nome: 'Outubro' },
      { id: 11, nome: 'Novembro' },
      { id: 12, nome: 'Dezembro' },
    ];
    return mesesDoAno[mes].nome;
  }
}
