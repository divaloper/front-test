import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FaturasComponent } from './pages/historico-geral/faturas.component';
import { HistoricoPorMesComponent } from './pages/historico-por-mes/historico-por-mes.component';
import { HistoricoPorCategoriaComponent } from './pages/historico-por-categoria/historico-por-categoria.component';

const routes: Routes = [
  { path: '', component: FaturasComponent },
  { path: 'geral', component: FaturasComponent },
  { path: 'mes', component: HistoricoPorMesComponent },
  { path: 'categoria', component: HistoricoPorCategoriaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
