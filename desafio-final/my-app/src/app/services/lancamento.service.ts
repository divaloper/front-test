import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class LancamentoService {
  url = 'https://desafio-it-server.herokuapp.com';

  constructor(private http: HttpClient) {}

  async getLancamentos(): Promise<any> {
    return await this.http.get(this.url + '/lancamentos').toPromise();
  }

  getCategorias(): Promise<any> {
    return this.http.get(this.url + '/categorias').toPromise();
  }
}
