import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Lancamento } from '../models/lancamento';
import { Categoria } from '../models/categoria';

const API = 'https://desafio-it-server.herokuapp.com';

@Injectable({ providedIn: 'root' })
export class LancamentoService {
  constructor(private http: HttpClient) {}

  getLancamentos(): Observable<Lancamento[]> {
    return this.http
      .get<Lancamento[]>(API + '/lancamentos')
      .pipe(retry(2), catchError(this.handleError));
  }

  getCategorias(): Observable<Categoria[]> {
    return this.http
      .get<Categoria[]>(API + '/categorias')
      .pipe(retry(2), catchError(this.handleError));
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
