import { Component, SimpleChanges, Input, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';

import { LancamentoService } from './services/lancamento.service';
import { Lancamento } from './models/lancamento';
import { Categoria } from './models/categoria';
import { GetMonth, likenMonths } from './class/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  lancamentos: Lancamento[];
  categorias: Categoria[];
  subs: Subscription;

  constructor(private lancamentoService: LancamentoService) {}

  ngOnInit() {
    this.getLancamentos();
    this.subs = this.lancamentoService.getCategorias().subscribe((categorias) => {
      this.categorias = categorias;
    });
  }

  ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  getLancamentos() {
    this.subs = this.lancamentoService.getLancamentos().subscribe((lancamentos) => {
      this.lancamentos = lancamentos;
      this.lancamentos.sort(likenMonths);
    });
  }

  getDescriptionCategory(category: number) {
    let description = 'Não informada';
    let nPos = this.categorias.findIndex((x) => category === x.id);

    if (nPos >= 0) {
      description = this.categorias[nPos].nome;
    }
    return description;
  }

  formatMonth(nMonth: number) {
    return GetMonth(nMonth);
  }
}
