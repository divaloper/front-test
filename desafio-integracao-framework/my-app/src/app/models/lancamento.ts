export class Lancamento {
  id: number;
  valor: number;
  origem: string;
  categoria: number;
  mes_lancamento: number;
  cMes_lancamento?: string;
  cCategoria?: string;
}
