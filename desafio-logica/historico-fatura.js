const MONTHS = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];

function BRLToCents(value) {
  return parseInt(value.replace(/R\$ +/gi, '').replace('.', '').replace(',', ''));
}

function centsToBRL(cents) {
  return Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  }).format(cents / 100);
}

function consolidatedInvoice(tableId) {
  var rows = Array.from(document.getElementById(tableId).rows);
  var consolidated = {};
  for (let i = 1; i < rows.length; i++) {
    var row = rows[i];
    var month = row.children[2].innerText;
    var valueInCents = BRLToCents(row.children[1].innerText);
    if (consolidated[month]) {
      consolidated[month] = consolidated[month] + valueInCents;
    } else {
      consolidated[month] = valueInCents;
    }
  }
  return consolidated;
}

function createTable() {
  var table = document.createElement('table');
  table.className = 'table';
  table.createTHead();
  table.createTBody();
  return table;
}

function createTR(type, ...values) {
  var tr = document.createElement('tr');

  values.forEach((value) => {
    var t = document.createElement(type);
    t.innerHTML = value;
    tr.appendChild(t);
  });

  return tr;
}

function createConsolidatedTable(consolidated) {
  var table = createTable();

  table.tHead.appendChild(createTR('th', 'Mês', 'Total Gasto'));

  var months = Object.keys(consolidated).sort();

  for (var i = 0; i < months.length; i++) {
    var month = months[i];
    var translatedMonth = MONTHS[parseInt(month) - 1];
    var totalForMonthInCents = consolidated[month];
    table.tBodies[0].appendChild(createTR('td', translatedMonth, centsToBRL(totalForMonthInCents)));
  }
  return table;
}

var body = document.getElementsByTagName('body')[0];
body.appendChild(document.createElement('br'));
body.appendChild(createConsolidatedTable(consolidatedInvoice('tb_fatura')));
